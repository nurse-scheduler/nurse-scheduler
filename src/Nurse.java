

/**
 * @author Ben
 *
 */
public class Nurse {
	private String name;
	private boolean priority;
	private Schedule request;
	private Schedule actual = null;

	public Nurse(String name, boolean isCharge, int day1, int day2) {
		super();
		this.name = name;
		this.priority = isCharge;
		this.request = new Schedule(day1, day2); 
	}
	
	public Nurse(String name, boolean isCharge, int day1, int day2, int day3) {
		super();
		this.name = name;
		this.priority = isCharge;
		this.request = new Schedule(day1, day2, day3); 
	}

	public Nurse() {
		// TODO Auto-generated constructor stub
	}

	public boolean isPriority() {
		return priority;
	}

	public Schedule getRequest() {
		return request;
	}

	public String getName() {
		return name;
	}

	public boolean isScheduled() {
		return actual != null;
	}

	public Schedule getActual() {
		return actual;
	}

	public void setActual(Schedule actual) {
		this.actual = actual;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPriority(boolean priority) {
		this.priority = priority;
	}

	public void setRequest(Schedule request) {
		this.request = request;
	}
	
	
}
