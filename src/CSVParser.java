import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;


public class CSVParser implements Parser {

	private Nurse parseLine(String line) {
		Nurse ret = new Nurse();
		String []elements = line.split(",");
		ret.setName(elements[0]);
		ret.setPriority("yes".equalsIgnoreCase(elements[1]));
		
		Schedule sched;
		if (elements.length == 4)
			sched = new Schedule(Integer.parseInt(elements[2]), 
								 Integer.parseInt(elements[3]));
		else
			sched = new Schedule(Integer.parseInt(elements[2]), 
					 Integer.parseInt(elements[3]),
					 Integer.parseInt(elements[4]));
		ret.setRequest(sched);
		return ret;
	}
	
	@Override
	public NurseList ParseNurses(Reader reader) {
		NurseList nlist= new NurseList();
		BufferedReader br = new BufferedReader(reader);
		String line = null;
		do {
			try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				return nlist;
			}
			Nurse nurse = parseLine(line);
			nlist.add(nurse);
		} while (line != null);
		return nlist;
	}

}
