import java.util.ArrayList;


public class Schedule extends ArrayList<Integer> { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Schedule(int day1, int day2) {
		this.add(day1);
		this.add(day2);
	}
	
	public Schedule(int day1, int day2, int day3) {
		this(day1, day2);
		this.add(day3);
	}

	@Override
	public String toString() {
		String ret = new String();
		for (Integer d : this) {
			ret += String.format("%d ", d.intValue());
		}
		return ret;
	}

}
