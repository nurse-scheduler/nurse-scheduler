import java.util.ArrayList;
import java.util.Iterator;


public class NurseList extends ArrayList<Nurse> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int avg_nurses = 30;
	
	public NurseList(int size) {
		super(size);
	}

	public NurseList() {
		this(avg_nurses);
	}

	public NurseList(NurseList nurses) {
		super(nurses);
	}

	public NurseList getChargeNurses() {
		NurseList ret = new NurseList(this.size());
		for (Nurse n : this) {
			if (n.isPriority())
				ret.add(n);
		}
		return ret;
	}

	@Override
	public String toString() {
		String ret = new String();
		for (Nurse n : this) {
			ret += n.getName() + ", ";
		}
		
		return ret.substring(0, ret.length()-2);
	}

	@Override
	public boolean equals(Object o) {

	    if (o == null) 
	    	return false;
	    if (o == this)
	    	return true;
	    if (!(o instanceof NurseList)) {
	    	return false;
	    }
	    
	    NurseList that = (NurseList)o;
	    
	    if (that.size() != this.size())
	    	return false;
	    
	    Iterator<Nurse> iter = this.iterator();
	    Iterator<Nurse> iter2 = that.iterator();
		while (iter.hasNext()) {
			if (iter.next() != iter2.next())
				return false;
		}
		
	    return true;
	}

}
