import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


public class Workweek {
	final int min_nurses = 1;
	final int max_nurses = 4;
	final int iterations = 10000000;
	Integer[] schedule = new Integer[] {0,0,0,0,0,0,0};
	
	private void set(NurseList nurses) {
	    for (Nurse n : nurses) {
	    	for (Integer d : n.getRequest()) {
	    		schedule[d]++;
	    		assert(!n.isScheduled());
	    		n.setActual(n.getRequest());
	    	}
	    }
	}
	
	/* Helper function to determine if we've gone over max nurses 
	 */
	private boolean above_max(Integer[] days) {
		for (Integer d : days)
	    	if (d > max_nurses)
	    		return true;
		
		return false;
	}
	
	/* Helper function to determine if we've gone over max nurses 
	 */
	private boolean below_min(Integer[] days) {
		for (Integer d : days)
	    	if (d < min_nurses)
	    		return true;
		
		return false;
	}
	
	/* Finds first failing day
	 * FIXME: yuck, -1 for success is lame 
	 */
	private int failing_day(Integer[] days) {
		for (Integer d : days) {
	    	if (d < min_nurses || d > max_nurses) {
	    		return d;
	    	}
	    }
		return -1;
	}
	
	/* Checks that there are no failing days */
	private boolean kosher(Integer[] days) {
		return !(above_max(days) || below_min(days));
	}
	

	private boolean good(NurseList nurses) {
		Integer[] days = schedule.clone();
		for (Nurse n : nurses) {
	    	for (Integer d : n.getRequest()) {
	    		days[d]++;
	    	}
	    }
		return kosher(days);
	}
	
	/* Recursively add requests until we fail, or find a good combo. If we find a good combo, keep going until we fail
	 * This operation destroys "nurses" 
	 * */
	private int unhappy_count(NurseList nurses, Integer[] days) {
		
		Nurse n = nurses.remove(0);
		
		/* exit condition: no more nurses, and not enough min */
		if (n == null && !kosher(days)) {
			return 10000;
		}

		/* Add the days this nurse requested to the temporary list */
		for (Integer d : n.getRequest()) {
			days[d]++;
		}
		
		/* If we've already gone over the max threshold, whatever remains is the best we can do */
		if (above_max(days))
			return nurses.size() + 1;
		else
			return unhappy_count(nurses, days);
	}


	private String[] do_build(NurseList nurses) {
		String[] line = new String[nurses.size()];
		int i = 0;
		for (Nurse n : nurses) {
			line[i++] = n.getName() + " " + n.getRequest() + "\n";
		}
		return line;
	}
	
	public String[] build(NurseList nurses) {
		/* Try to make everyone happy first */
		if (this.good(nurses))
			return do_build(nurses);
		
		set(nurses.getChargeNurses());
		if (above_max(schedule)) {
			System.err.println("Charge nurse fail");
			return null;
		}
		
		// Charge nurses are done. Take em out.
		nurses.removeAll(nurses.getChargeNurses());
		
		int unhappy = nurses.size();
		ArrayList<NurseList> potential_winners = new ArrayList<>();
		NurseList temp;
		System.out.println("Starting the search with " + unhappy + " unhappy nurses");
		
		/* Going through all permutations is computationally impossible. Instead we hackily randomize our list a bunch of times and hope for the best. */
		
		for (int i = 0; i < iterations; i++) {
			Collections.shuffle(nurses);
			temp = new NurseList(nurses);
			if (above_max(schedule.clone())) {
				System.err.println("something is very wrong");
			}
			int temp_count = unhappy_count(temp, schedule.clone());
			temp = new NurseList(nurses);
			if (temp_count <= 0 || temp.size() - temp_count == 7) {
				System.err.println("something is very wrong2");
			}
			/* Collect every good possibility so we can give the user some options later */
			if (temp_count <= unhappy) {
				temp.removeAll(temp.subList(0, temp.size() - temp_count));
				/* Create two lists, the happy nurses, and the unhappy nurses */
				potential_winners.add(new NurseList(temp));

				unhappy = temp_count;
			}
		}
		
		/* Trim false positives we recorded earlier */
		Iterator<NurseList> iter = potential_winners.listIterator();
		while (iter.hasNext()) {
			NurseList nlist = iter.next();
			if (nlist.size() != unhappy) {
				iter.remove();
			} else {
				//System.out.println(nlist);
			}
		}
		
		// Trick to remove duplicates (requires that we override equals())
		Set<NurseList> templist = new LinkedHashSet<NurseList>(potential_winners);
		potential_winners.clear();
		potential_winners.addAll(templist);
		
		System.out.println("There are " + potential_winners.size() + " unique choices");
		/*
		for (NurseList nlist : potential_winners) {
			System.out.print("If you were to remove " + nlist + "\n");
			NurseList temp1 = new NurseList(nurses);
			temp1.removeAll(nlist);
			System.out.println(Arrays.toString(do_build(temp1)));
			System.out.print(nurses + "\n\n");
		}
		*/
		
		return null;
	}

	
}
