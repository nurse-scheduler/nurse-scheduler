import java.util.Arrays;



public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Parser p = new CSVParser();
		NurseList nurses = p.ParseNurses(null);
		Workweek ww = new Workweek();
		System.out.print(Arrays.toString(ww.build(nurses)));
	}

}
